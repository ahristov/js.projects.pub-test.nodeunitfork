# Nodeunit

"Simple syntax, powerful tools.
**Nodeunit** provides easy async unit testing
for node.js and the browser."

> URL: https://github.com/caolan/nodeunit



## Changes

Trying to log into the jUnit reports, thereof created fork here:

> https://github.com/atanashristov/nodeunit



## Node modules

### Add node modules

Added node_modules:

	mkdir node_modules
	git submodule add git://github.com/atanashristov/nodeunit.git node_modules/nodeunit

Note: To remove a submodule from git repository:

- Delete the relevant line from the .gitmodules file.
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).

Example:

	git rm --cached node_modules/shred

- Commit to git
- Delete the now untracked submodule files.


### Download node modules

When cloning the repository, download by doing:

	git submodule init
	git submodule update




## Running the tests

Running the tests from command prompt:


	$ node run-tests.js



