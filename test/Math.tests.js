/**
 * Created by JetBrains WebStorm.
 * User: ahristov
 * Date: 3/27/12
 * Time: 12:52 PM
 * To change this template use File | Settings | File Templates.
 */


module.exports = {

	setUp: function (callback) {
//		this.request = require('request');
//		this.underscore = require('underscore');
//		this.qs = require('querystring');


		callback();
	},
	tearDown: function (callback) {
//		delete this.request;
//		delete this.underscore;
//		delete this.qs;

		callback();
	},

	"test - 1 plus 1 - should make 2" : function(test) {

		test.expect(2);

		var sum = 1 + 1;

		test.log("testing if sum > 0");
		test.ok(sum > 0);

		test.log("testing if sum == 2");
		test.ok(sum == 2);

		test.done();
	}
}